##**Firefly - A discord bot.**

Pardon the dust. This bot is still under construction.

This bot is being custom made to aid in moderation and support in the Discord server for a minecraft realms network.  
It is still in the early days of development.  


---

## Current Status:  
####Working features:  
1. ping  - ping the bot to see if it's working
2. help  - displays a list of commands and how to use them
3. report  - starts a dialog in the user's dms to gather info to make a support ticket, then sends that report ticket to a "reports" discord channel for admins to see.
  
####Features actively in development: 
4. about
5. feedback
  
####Some planned features comming soon:  
1. reportRespond  - allows an admins to respond to a ticket through the bot back to the user's dms
2. reportQuery  - allows an admin to search through support tickets based on different criteria

and more...  
  
---
